﻿using Microsoft.EntityFrameworkCore;
using Notes.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Notes.DatabaseInteraction.EntityTypeConfigurations
{
    class NoteConfiguration : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.HasKey(note => note.Id);
            builder.HasIndex(note => note.Id).IsUnique();
            builder.Property(note => note.Title).HasMaxLength(250);
        }
    }
}
